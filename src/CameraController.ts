const clamp = (value: number, min: number, max: number) => {
  return Math.min(Math.max(value, min), max);
};

declare const Potree: any;
declare const THREE: any;
declare const TWEEN: any;

const { MOUSE, Utils, EventDispatcher } = Potree;

type DragEventType = {
  drag: {
    object: null;
    startHandled: boolean | undefined;
    lastDrag: { x: number; y: number };
    mouse: any;
  };
};

const keys = {
  FORWARD: ['W'.charCodeAt(0), 38],
  BACKWARD: ['S'.charCodeAt(0), 40],
  LEFT: ['A'.charCodeAt(0), 37],
  RIGHT: ['D'.charCodeAt(0), 39],
  UP: ['E'.charCodeAt(0), 33],
  DOWN: ['Q'.charCodeAt(0), 34],
};

const isTruthyKey = (keyCode: number) => {
  return Object.values(keys).some((key) => key.some((value) => value === keyCode));
};

export class OrbitControls extends EventDispatcher {
  viewer: any;
  renderer: any;
  scene: any;

  moveSpeedFactor = 2;
  rotationSpeed = 150;
  fadeFactor = 10;
  yawDelta = 0;
  pitchDelta = 0;
  radiusDelta = 0;
  minSpeed = 0.1;
  maxSpeed = 100;
  minZLevel = 15;

  isKeyPressed: number | null = null;

  panDelta: any;
  translationDelta: any;
  translationWorldDelta: any;

  doubleClockZoomEnabled = true;
  tweens: any[];
  previousTouch: { touches: string | any[] } | null = null;
  previousZ = 0;

  constructor(viewer: { renderer: any }) {
    super();

    this.viewer = viewer;
    this.renderer = viewer.renderer;

    this.scene = null;
    this.sceneControls = new THREE.Scene();

    this.panDelta = new THREE.Vector2(0, 0);

    this.tweens = [];
    this.translationDelta = new THREE.Vector3(0, 0, 0);
    this.translationWorldDelta = new THREE.Vector3(0, 0, 0);

    this.addEventListener('touchstart', this.touchStart);
    this.addEventListener('touchend', this.touchEnd);
    this.addEventListener('touchmove', this.touchMove);
    this.addEventListener('drag', this.drag);
    this.addEventListener('drop', this.drop);
    this.addEventListener('mousewheel', this.scroll);
    this.addEventListener('dblclick', this.dblclick);

    this.viewer.scene.addEventListener('pointcloud_added', () => {
      this.maxSpeed = this.viewer.moveSpeed * 2;
      this.viewer.setMoveSpeed(this.maxSpeed);
    });

    const {
      inputHandler: { domElement },
      inputHandler,
    } = this.viewer;

    domElement.addEventListener('blur', () => {
      if (this.isKeyPressed && isTruthyKey(this.isKeyPressed)) {
        this.isKeyPressed = null;
        inputHandler.pressedKeys = [];

        this.dispatchEvent({
          type: 'end',
        });
      }
    });

    domElement.addEventListener('keydown', (e: KeyboardEvent) => {
      if (!this.isKeyPressed) {
        this.isKeyPressed = e.keyCode;

        if (isTruthyKey(this.isKeyPressed)) {
          this.dispatchEvent({
            type: 'start',
          });
        }
      }
    });

    domElement.addEventListener('keyup', () => {
      this.isKeyPressed = null;

      this.dispatchEvent({
        type: 'end',
      });
    });
  }

  drag(e: DragEventType) {
    if (e.drag.object !== null) {
      return;
    }

    if (e.drag.startHandled === undefined) {
      e.drag.startHandled = true;

      this.dispatchEvent({ type: 'start' });
    }

    const moveSpeed = this.viewer.getMoveSpeed();

    const ndrag = {
      x: e.drag.lastDrag.x / this.renderer.domElement.clientWidth,
      y: e.drag.lastDrag.y / this.renderer.domElement.clientHeight,
    };

    if (e.drag.mouse === MOUSE.LEFT) {
      this.yawDelta += ndrag.x * this.rotationSpeed;
      this.pitchDelta += ndrag.y * this.rotationSpeed;
    } else if (e.drag.mouse === MOUSE.RIGHT) {
      this.translationDelta.x -= ndrag.x * moveSpeed * 100;
      this.translationDelta.z += ndrag.y * moveSpeed * 100;
    }
  }

  drop() {
    this.dispatchEvent({ type: 'end' });
  }

  dblclick(e: { mouse: any }) {
    if (this.doubleClockZoomEnabled) {
      this.zoomToLocation(e.mouse);
    }
  }

  touchStart(e: any) {
    this.previousTouch = e;
  }

  touchEnd(e: any) {
    this.previousTouch = e;
  }

  scroll(e: { delta: number }) {
    const resolvedRadius = this.viewer.getMoveSpeed() + this.radiusDelta;
    this.radiusDelta += -e.delta * resolvedRadius * 0.1;

    this.stopTweens();
  }

  changeSpeedByScrolling(delta: number) {
    let speed = this.viewer.getMoveSpeed();

    if (delta < 0) {
      speed *= 0.9;
    } else if (delta > 0) {
      speed /= 0.9;
    }

    speed = clamp(speed, this.minSpeed, this.maxSpeed);

    this.dispatchEvent({
      type: 'moveSpeedChange',
      speed,
    });

    this.viewer.setMoveSpeed(speed);
  }

  touchMove(e: { touches: string | any[] }) {
    if (!this.previousTouch) {
      return;
    }

    if (e.touches.length === 2 && this.previousTouch.touches.length === 2) {
      const prev = this.previousTouch;
      const curr = e;

      const prevDX = prev.touches[0].pageX - prev.touches[1].pageX;
      const prevDY = prev.touches[0].pageY - prev.touches[1].pageY;
      const prevDist = Math.sqrt(prevDX * prevDX + prevDY * prevDY);

      const currDX = curr.touches[0].pageX - curr.touches[1].pageX;
      const currDY = curr.touches[0].pageY - curr.touches[1].pageY;
      const currDist = Math.sqrt(currDX * currDX + currDY * currDY);

      const delta = currDist / prevDist;
      const resolvedRadius = this.scene.view.radius + this.radiusDelta;
      const newRadius = resolvedRadius / delta;
      this.radiusDelta = newRadius - resolvedRadius;

      this.stopTweens();
    } else if (e.touches.length === 3 && this.previousTouch.touches.length === 3) {
      const prev = this.previousTouch;
      const curr = e;

      const prevMeanX = (prev.touches[0].pageX + prev.touches[1].pageX + prev.touches[2].pageX) / 3;
      const prevMeanY = (prev.touches[0].pageY + prev.touches[1].pageY + prev.touches[2].pageY) / 3;

      const currMeanX = (curr.touches[0].pageX + curr.touches[1].pageX + curr.touches[2].pageX) / 3;
      const currMeanY = (curr.touches[0].pageY + curr.touches[1].pageY + curr.touches[2].pageY) / 3;

      const delta = {
        x: (currMeanX - prevMeanX) / this.renderer.domElement.clientWidth,
        y: (currMeanY - prevMeanY) / this.renderer.domElement.clientHeight,
      };

      this.panDelta.x += delta.x;
      this.panDelta.y += delta.y;

      this.stopTweens();
    }

    this.previousTouch = e;
  }

  setScene(scene: any) {
    this.scene = scene;
  }

  stop() {
    this.yawDelta = 0;
    this.pitchDelta = 0;
    this.radiusDelta = 0;
    this.panDelta.set(0, 0);
  }

  zoomToLocation(mouse: any) {
    const camera = this.scene.getActiveCamera();

    const intersection = Utils.getMousePointCloudIntersection(
      mouse,
      camera,
      this.viewer,
      this.scene.pointclouds,
      { pickClipped: true },
    );

    if (intersection === null) {
      return;
    }

    let targetRadius = 0;
    const minimumJumpDistance = 0.2;

    const { domElement } = this.renderer;
    const ray = Utils.mouseToRay(mouse, camera, domElement.clientWidth, domElement.clientHeight);

    const targetPointCloud = intersection.pointcloud;
    const nodes = targetPointCloud.nodesOnRay(targetPointCloud.visibleNodes, ray);
    const lastNode = nodes[nodes.length - 1];
    const { radius } = lastNode.getBoundingSphere(new THREE.Sphere());
    targetRadius = Math.min(this.scene.view.radius, radius);
    targetRadius = Math.max(minimumJumpDistance, targetRadius);

    const d = this.scene.view.direction.multiplyScalar(-1);
    const cameraTargetPosition = new THREE.Vector3().addVectors(
      intersection.location,
      d.multiplyScalar(targetRadius),
    );

    this.animateZoom(cameraTargetPosition, intersection.location);
  }

  animateZoom(cameraTargetPosition: any, intersectionLocation: any) {
    const animationDuration = 600;
    const easing = TWEEN.Easing.Quartic.Out;

    // animate
    const value = { x: 0 };
    const tween = new TWEEN.Tween(value).to({ x: 1 }, animationDuration);
    tween.easing(easing);
    this.tweens.push(tween);

    const startPos = this.scene.view.position.clone();
    const targetPos = cameraTargetPosition.clone();
    const startRadius = this.scene.view.radius;
    const targetRadius = cameraTargetPosition.distanceTo(intersectionLocation);

    tween.onUpdate(() => {
      const t = value.x;
      this.scene.view.position.x = (1 - t) * startPos.x + t * targetPos.x;
      this.scene.view.position.y = (1 - t) * startPos.y + t * targetPos.y;
      this.scene.view.position.z = (1 - t) * startPos.z + t * targetPos.z;

      this.scene.view.radius = (1 - t) * startRadius + t * targetRadius;
      this.viewer.setMoveSpeed(this.scene.view.radius / 2.5);
    });

    tween.onComplete(() => {
      this.tweens = this.tweens.filter((x) => x !== tween);
    });

    tween.start();
  }

  stopTweens() {
    this.tweens.forEach((x) => x.stop());
    this.tweens = [];
  }

  update(delta: number) {
    const { view } = this.scene;

    this.cancelMoveAnimations();
    this.calculateKeyboardTranslation();
    this.applyRotationTransformation(view, delta);
    this.applyGeneralTranslation(view, delta);
    this.zoomingScroll(view, delta);

    this.speedByZProcessing(view.position.z);

    this.decelerate(delta);
  }

  zoomingScroll(view: any, delta: number) {
    const progression = Math.min(1, this.fadeFactor * delta);
    const radius = view.radius + progression * this.radiusDelta;
    const V = view.direction.multiplyScalar(-radius);
    const position = new THREE.Vector3().addVectors(view.getPivot(), V);

    if (position.z < this.minZLevel) {
      position.z = this.minZLevel;
    }

    view.position.copy(position);
  }

  speedByZProcessing(z: number) {
    if (this.previousZ && this.previousZ !== z) {
      const speed = this.viewer.getMoveSpeed();
      const diff = (this.previousZ - z) / z;
      const newSpeed = speed - speed * diff;

      this.viewer.setMoveSpeed(newSpeed);
    }

    this.previousZ = z;
  }

  applyRotationTransformation(view: any, delta: number) {
    let { yaw, pitch } = view;

    yaw -= this.yawDelta * delta;
    pitch -= this.pitchDelta * delta;

    view.yaw = yaw;
    view.pitch = pitch;
  }

  calculateKeyboardTranslation() {
    const ih = this.viewer.inputHandler;

    const moveForward = keys.FORWARD.some((x) => ih.pressedKeys[x]);
    const moveBackward = keys.BACKWARD.some((x) => ih.pressedKeys[x]);
    const moveLeft = keys.LEFT.some((x) => ih.pressedKeys[x]);
    const moveRight = keys.RIGHT.some((x) => ih.pressedKeys[x]);
    const moveUp = keys.UP.some((x) => ih.pressedKeys[x]);
    const moveDown = keys.DOWN.some((x) => ih.pressedKeys[x]);

    if (moveForward && moveBackward) {
      this.translationDelta.y = 0;
    } else if (moveForward) {
      this.translationDelta.y = this.viewer.getMoveSpeed();
    } else if (moveBackward) {
      this.translationDelta.y = -this.viewer.getMoveSpeed();
    }

    if (moveLeft && moveRight) {
      this.translationDelta.x = 0;
    } else if (moveLeft) {
      this.translationDelta.x = -this.viewer.getMoveSpeed();
    } else if (moveRight) {
      this.translationDelta.x = this.viewer.getMoveSpeed();
    }

    if (moveUp && moveDown) {
      this.translationWorldDelta.z = 0;
    } else if (moveUp) {
      this.translationWorldDelta.z = this.viewer.getMoveSpeed();
    } else if (moveDown) {
      this.translationWorldDelta.z = -this.viewer.getMoveSpeed();
    }
  }

  translate(x: number, y: number, z: number) {
    const scene = this.scene.view;
    const dir = new THREE.Vector3(0, 1, 0);
    dir.applyAxisAngle(new THREE.Vector3(1, 0, 0), scene.pitch);
    dir.applyAxisAngle(new THREE.Vector3(0, 0, 1), scene.yaw);

    const side = new THREE.Vector3(1, 0, 0);
    side.applyAxisAngle(new THREE.Vector3(0, 0, 1), scene.yaw);

    const up = side.clone().cross(dir);

    const scalar = dir.multiplyScalar(y);

    if (scene.position.z < this.minZLevel && y > 0) {
      scalar.z = 0;
    }

    const t = side.multiplyScalar(x).add(scalar).add(up.multiplyScalar(z));

    this.position = scene.position.add(t);
  }

  applyGeneralTranslation(view: any, delta: number) {
    this.translate(
      this.translationDelta.x * delta,
      this.translationDelta.y * delta,
      this.translationDelta.z * delta,
    );

    let z = this.translationWorldDelta.z * delta;
    if (view.position.z < this.minZLevel && z < 0) {
      z = 0;
    }

    view.translateWorld(
      this.translationWorldDelta.x * delta,
      this.translationWorldDelta.y * delta,
      z,
    );
  }

  decelerate(delta: number) {
    const progression = Math.min(1, this.fadeFactor * delta);
    const attenuation = Math.max(0, 1 - this.fadeFactor * delta);
    this.yawDelta *= attenuation;
    this.pitchDelta *= attenuation;
    this.translationDelta.multiplyScalar(attenuation);
    this.translationWorldDelta.multiplyScalar(attenuation);
    this.radiusDelta -= progression * this.radiusDelta;
  }

  cancelMoveAnimations() {
    const changes = [
      this.yawDelta,
      this.pitchDelta,
      this.translationDelta.length(),
      this.translationWorldDelta.length(),
    ];
    const changeHappens = changes.some((e) => Math.abs(e) > 0.001);
    if (changeHappens && this.tweens.length > 0) {
      this.tweens.forEach((e) => e.stop());
      this.tweens = [];
    }
  }
}
