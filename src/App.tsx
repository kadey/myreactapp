// import React from 'react';
import { OrbitControls } from './CameraController'

declare const Potree: any;
declare const THREE: any;

function App() {
  return (
    <div className="App">
    </div>
  );
}

setTimeout(e => {

  const w = (window as any);
  w.viewer = new Potree.Viewer(document.querySelector('.App'));
  w.viewer.setFOV(60);
  w.viewer.setClipTask(Potree.ClipTask.SHOW_OUTSIDE);
  const controls = new OrbitControls(w.viewer);
  controls.addEventListener('start', () => w.viewer.disableAnnotations());
  controls.addEventListener('end', () => w.viewer.enableAnnotations());
  w.viewer.setControls(controls);

  Potree.loadPointCloud("https://stagegpdm.rubius.com//pointclouds/cloud-4/metadata.json", 'scene', (e: any) => {
    const { pointcloud } = e;
    
    const { material } = pointcloud;
    material.size = 1;
    material.pointSizeType = Potree.PointSizeType.ADAPTIVE;
    material.activeAttributeName = 'rgba';

    w.viewer.scene.addPointCloud(pointcloud);

    const { pointclouds } = w.viewer.scene;
    const box = w.viewer.getBoundingBox(pointclouds);
    box.set(box.min, new THREE.Vector3(box.max.x, box.max.y, box.max.z / 2));
    const node = new THREE.Object3D();
    node.boundingBox = box;
    w.viewer.zoomTo(node, 0.6);

    w.viewer.inputHandler.domElement.focus();
  });

}, 1000);

export default App;
